ADDRESS PRINTING TEMPLATE
=========================

Use Case
--------
A fashion online shop have 2 delivery service, such as:

1. direct: sent the item to customer directly
2. dropship: sent the item to other people customer.

This app configured to make this job easier. There are 3 x 4 address form (for letter paper), and in every address form there are 2 field such as "from" and "to". If you want to use direct ship you only need to fill "to" field, but if you want use dropship you need to fill "from" field too.

Dependency
----------
PHP and PHP-GD Library

