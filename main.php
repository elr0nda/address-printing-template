<?php
/** setting **/

define(MAX_COL, 3);
define(MAX_ROW, 4);

define(IMAGE_WIDTH, "250px");
define(IMAGE_HEIGHT, "250px");

define(TO_X_DROPSHIP, 95);
define(TO_Y_DROPSHIP, 87);

define(FROM_X_DROPSHIP, 87);
define(FROM_Y_DROPSHIP, 415);

define(TO_X, 80);
define(TO_Y, 93);

define(FONT_SIZE, 18.0);

define(SPACE, 6.0);

define(TEMP_FILE_PATH, "/var/www/tools/address/tempfile/");
define(FONT_PATH, "/var/www/tools/address/Calibri.ttf");
define(IMAGE_DROPSHIP, "/var/www/tools/address/dropship.jpg");
define(IMAGE_PLAIN, "/var/www/tools/address/plain.jpg");
?>

<?php if(!isset($_POST["process"])) {?>

<?php
function show_input_form ($index) {
	?><table>
	<tr><td>Alamat </td><td><?php echo $index; ?></td></tr>
	<tr><td>From</td><td><textarea name="from[]" rows="2" cols="15"></textarea></td></tr>
	<tr><td>To</td><td><textarea name="to[]" rows="8" cols="30"></textarea></td></tr>
	</table>
	<?php
}
?>

<form method="POST" action="main.php">
<input type="submit" name="process" value="process"/>
<table>
<?php 
for($row = 0; $row < MAX_ROW; ++$row) {
	echo "<tr>";
	for($col = 0; $col < MAX_COL; ++$col) {
		echo "<td>";
		show_input_form($row * MAX_COL + $col + 1);
		echo "</td>";
	}
	echo "</tr>";
} 
?>
</table>
</form>
<?php } else { ?>
<?php 
	$from = $_POST["from"];
	$to = $_POST["to"];

	$end = FALSE; 

	echo "<table>";
	for($row = 0; $row < MAX_ROW; ++$row)
	{
		echo "<tr>";
		for($col = 0; $col < MAX_COL; ++$col)
		{
			$index = $row * MAX_COL + $col;
			$content = "";
			if(strlen(trim($from[$index])) == 0 && strlen(trim($to[$index])) == 0)
			{
				$end = TRUE;
			}
			else
			{
				$content = "<img style='width:" . IMAGE_WIDTH . ";height:" . IMAGE_HEIGHT . "' src='" . "tempfile/" . process_image($from[$index], $to[$index]) . "'/>";
			}
			echo "<td>" . $content . "</td>";
		}
		echo "</tr>";

		if($end) break;
	}
	echo "</table>";
	
?>
<?php } ?>

<?php 
function process_image ($from, $to) {

	$is_dropship = false;
	
	$image_path = IMAGE_PLAIN; 
	if(strlen(trim($from)) > 0)
	{
		$is_dropship = true;
		$image_path = IMAGE_DROPSHIP; 
	}

	$font_path = FONT_PATH; 

	$image = imagecreatefromjpeg($image_path);

	if($is_dropship)
	{
		imagettftextsp($image, FONT_SIZE, 0.0, TO_X_DROPSHIP, TO_Y_DROPSHIP, 0, $font_path, $to, SPACE);
		imagettftextsp($image, FONT_SIZE, 0.0, FROM_X_DROPSHIP, FROM_Y_DROPSHIP, 0, $font_path, $from, SPACE);
	}
	else
	{
		imagettftextsp($image, FONT_SIZE, 0.0, TO_X, TO_Y, 0, $font_path, $to, SPACE);
	}

	$path = TEMP_FILE_PATH;

	$filename = sha1(rand() . $from . $to . rand()) . ".jpeg";

	imagejpeg($image, $path . $filename);

	imagedestroy($image);

	return $filename;
}

function imagettftextsp ($image, $font_size, $angle, $x, $y, $color, $font_path, $text, $space) {

	$bbox = imagettfbbox($font_size, $angle, $font_path, "pl");
	$font_height = $bbox[1] - $bbox[7];

	$text = explode("\n", $text);

	$rows = count($text);

	for($i = 0; $i < $rows; ++$i)
	{
		imagettftext($image, $font_size, $angle, $x, $y, $color, $font_path, $text[$i]);
		$y += $font_height + $space;
	}

}
?>



